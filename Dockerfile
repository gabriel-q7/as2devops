FROM node:latest as node
WORKDIR /appDocker
COPY . .
RUN npm ci
RUN npm run build --prod
