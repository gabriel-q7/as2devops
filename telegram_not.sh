#!/bin/sh

BOT_URL="https://api.telegram.org/bot${TELEGRAM_TOKEN}/sendMessage"

echo "${CI_JOB_STATUS}"

MESSAGE="
-------------------------------------
(
Gitlab build: * ${CI_JOB_STATUS}!*
\`Repository:  ${CI_PROJECT_DIR}\`
\`Branch:      ${CI_COMMIT_BRANCH}\`
*Commit Msg:*
${CI_COMMIT_MESSAGE}
)
-------------------------------------
"

curl -s -X POST ${BOT_URL} -d chat_id="848119339" -d text="${MESSAGE}" -d parse_mode="Markdown"
